'use strict';

const debug = require('debug')('myapp');
const globalVariables = require('../../lib/utils/global-variables.js');
const utils = require('../../lib/utils/utils.js');
const express = require('express');
const Promise = require('bluebird');
const _ = require('lodash');
const consts = require('../../lib/consts/consts');
const config = require('config');
const validator = require('validator');
const mongoose = require('mongoose');
const async = require('async');
const Schedule = require('../../lib/schedule/scheduleClass.js');
const logUtils = require('../../lib/utils/logUtils.js');

const router = express.Router();

// Delete a push
router.delete('/push/:_id', validate, function(req, res, next){
	const _id = req.params._id;

	globalVariables
		.get('models')
		.PushNotification
		.remove({
			_id: _id
		})
    .exec()
		.then(function(r){
      logUtils.logInfo(r);
      
			res.json({
				ec: consts.CODE.SUCCESS,
			});

			// Emit an event about delete a push
			Schedule.emit('job removed', {idJob: _id});
		})
		.catch(err => {
			next(err);
		});
});

// Middleware for checking data
function validate(req, res, next) {
  const _id = req.params._id;

  if(!validator.isMongoId(_id)) {
    res.json({
      ec: consts.CODE.WRONG_PARAM
    })
    return next('route');
  }

  next();
}

module.exports = function (app) {
  app.use('/', router);
};
