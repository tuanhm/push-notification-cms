'use strict';

const debug = require('debug')('myapp');
const globalVariables = require('../../lib/utils/global-variables.js');
const utils = require('../../lib/utils/utils.js');
const express = require('express');
const Promise = require('bluebird');
const _ = require('lodash');
const consts = require('../../lib/consts/consts');
const config = require('config');
const validator = require('validator');
const mongoose = require('mongoose');
const async = require('async');
const Schedule = require('../../lib/schedule/scheduleClass.js');
const logUtils = require('../../lib/utils/logUtils.js');

const router = express.Router();

router.post('/push', validate, function (req, res, next){
	const oss = req.body.oss;
	const content = req.body.content;
	const timeStart = req.body.timeStart;
	const idApp = req.body.idApp;
	const title = req.body.title;
	const data = req.body.data;
	console.log(data);
	globalVariables
		.get('models')
		.PushNotification
		.create({
			oss: oss,
			idApp: idApp,
			content: content,
			timeStart: timeStart,
			title: title,
			data: data,
			status: consts.PUSH_STATE.PENDDING
		})
		.then(r => {
      logUtils.logInfo(r);

			res.json({
				ec: consts.CODE.SUCCESS,
				id: r._id
			});

			// Emit an event to Schedule module
			Schedule.emit('job created', {
				idJob: r._id.toString(),
				timeStart: timeStart
			});
		})
		.catch(err => {
			next(err);
		});
});

function validate(req, res, next) {
  const oss = req.body.oss;
	const content = req.body.content;
	const timeStart = req.body.timeStart;
	const idApp = req.body.idApp;
	const title = req.body.title;
	const data = req.body.data;

  if(!_.isInteger(oss)
  || !(_.isString(content) && validator.isLength(content.trim(), 1))
  || !_.isInteger(timeStart)
  || !_.isInteger(idApp)
  || !_.isString(title) ) {
    res.json({
      ec: consts.CODE.WRONG_PARAM
    });
    return next('route');
  }

  // Standardize
  req.body.content = content.trim();
  req.body.title = title.trim();

  if(!_.isPlainObject(data)) {
    req.body.data = {}
  }

  next();
}

module.exports = function (app) {
  app.use('/', router);
};
