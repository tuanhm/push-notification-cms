'use strict';

const debug = require('debug')('myapp');
const globalVariables = require('../../lib/utils/global-variables.js');
const utils = require('../../lib/utils/utils.js');
const express = require('express');
const Promise = require('bluebird');
const _ = require('lodash');
const consts = require('../../lib/consts/consts');
const config = require('config');
const validator = require('validator');
const mongoose = require('mongoose');
const async = require('async');
const Schedule = require('../../lib/schedule/scheduleClass.js');
const logUtils = require('../../lib/utils/logUtils.js');

const router = express.Router();

// Modify a push, only modify if that push is in 'PENNDING' or 'NOT_YET' state
router.put('/push/:_id', validate, function(req, res, next){
  const _id = req.params._id;
  const timeStart = req.body.timeStart;

  getPushInf(_id)
    .then(r => {
      logUtils.logInfo(r);

      if(r !== null) {
        const status = r.status;
				if ( (status === consts.PUSH_STATE.PENDDING) || (status === consts.PUSH_STATE.NOT_YET)) {
					if (status === consts.PUSH_STATE.NOT_YET) {
						req.body.status = consts.PUSH_STATE.PENDDING;
					}
          return updatePushInf(req.body);
        } else {
          return Promise.reject({
            ec: consts.CODE.FAIL
          });
        }
      } else {
        return Promise.reject({
          ec: consts.CODE.FAIL
        });
      }
    })
    .then(r => {
      logUtils.logInfo(r);

      res.json({
        ec: consts.CODE.SUCCESS
      });

      if(timeStart !== r.timeStart){
        Schedule.emit('job modified', {idJob: _id, timeStart: timeStart});
      }
    })
    .catch(err => {
      if(_.isError(err)) {
        return next(err);
      }

      res.json(err);
    });
});

function getPushInf(_id) {
  return globalVariables
		.get('models')
		.PushNotification
		.findOne({
			_id: _id
		})
    .lean()
    .exec();
}

function updatePushInf(obj) {
  return globalVariables
          .get('models')
          .PushNotification
          .findOneAndUpdate({
            _id: _id
          }, obj, {
            new: false
          })
          .exec();
}

// Middleware for checking data
function validate(req, res, next) {
  const _id = req.params._id;
  const oss = req.body.oss;
	const content = req.body.content;
	const timeStart = req.body.timeStart;
	const timeEnd = req.body.timeEnd;
	const idApp = req.body.idApp;
	const title = req.body.title;
	const param = req.body.param;
  const link = req.body.link;
  const extras = req.body.extras;

  if(!_.isMongoId(_id)
  || !_.isInteger(oss)
  || !(_.isString(content) && validator.isLength(content.trim(), 1))
  || !_.isInteger(timeStart)
  || !_.isInteger(timeEnd)
  || !_.isInteger(idApp)
  || !_.isString(title) ) {
    res.json({
      ec: consts.CODE.WRONG_PARAM
    });
    next('route');
  }

  // Standardize
  req.body.content = content.trim();
  req.body.title = title.trim();

  if(_.isString(param)) {
    req.body.param = param.trim();
  } else {
    req.body.param = '';
  }

  if(_.isString(link)) {
    req.body.link = link.trim();
  } else {
    req.body.link = '';
  }

  if(!_.isPlainObject(extras)) {
    req.body.extras = {}
  }

  next();
}


module.exports = function (app) {
  app.use('/', router);
};
