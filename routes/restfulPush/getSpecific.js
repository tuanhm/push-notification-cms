'use strict';

const debug = require('debug')('myapp');
const globalVariables = require('../../lib/utils/global-variables.js');
const utils = require('../../lib/utils/utils.js');
const express = require('express');
const Promise = require('bluebird');
const _ = require('lodash');
const consts = require('../../lib/consts/consts');
const config = require('config');
const validator = require('validator');
const mongoose = require('mongoose');
const async = require('async');
const logUtils = require('../../lib/utils/logUtils.js');

const router = express.Router();

// Get inf about a push
router.get('/push/:_id', validate, function(req, res, next){
	const _id = req.params._id;

  let pushInf = null;

  getPushInf(_id)
		.then(r => {
			logUtils.logInfo(r);

      if(r !== null && r.status === consts.PUSH_STATE.SUCCESS) {
        pushInf = r;
        return getResultPush(_id);
      } else {
        return Promise.reject({
          ec: consts.CODE.SUCCESS,
          inf: r
        })
      }
		})
    .then(r => {
			logUtils.logInfo(r);

      pushInf.pushResult = r;
      res.json({
        ec: consts.CODE.SUCCESS,
        inf: pushInf
      })
    })
		.catch(err => {
      if(_.isError(err)) {
				return next(err);
      }

      res.json(err);
		});
});

// Middleware for checking data
function validate(req, res, next) {
  const _id = req.params._id;

  if(!validator.isMongoId(_id)) {
    res.json({
      ec: consts.CODE.WRONG_PARAM
    })
    return next('route');
  }

  next();
}

function getPushInf(_id) {
  return globalVariables
		.get('models')
		.PushNotification
		.findOne({
			_id: _id
		})
    .lean()
    .exec();
}

function getResultPush(idPush) {
  return new Promise((resolve, reject) => {
    globalVariables
  		.get('models')
  		.PushResult
  		.findOne({
  			idPush: idPush
  		}, {
        idPush: 0,
        errorMessage: 0,
        _id: 0
      })
      .lean()
      .exec()
      .then(r => {
        // More handle here
        resolve(r);
      })
      .catch(err => {
        reject(err);
      });
  })
}

function handleErrorMessage(errors) {
  const dataReturn = [];
  errors.forEach(error => {
    if(error !== '') {
      const index = findElement(error, dataReturn);
      if(index === -1) {
        const element = {
          message: error,
          times: 1
        }
        dataReturn.push(element);
      } else {
        dataReturn[index][times] = dataReturn[index][times] + 1;
      }
    }
  });
  return dataReturn;
}

function findElement(error, data) {
  let index = -1;
  const length = data.length;
  for(let i=0; i<length; i++) {
    if(data[i].message === error) {
      index = i;
      break;
    }
  }
  return index;
}

module.exports = function (app) {
  app.use('/', router);
};
