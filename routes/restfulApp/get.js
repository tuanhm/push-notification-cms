'use strict';

const debug = require('debug')('myapp');
const globalVariables = require('../../lib/utils/global-variables.js');
const utils = require('../../lib/utils/utils.js');
const express = require('express');
const Promise = require('bluebird');
const _ = require('lodash');
const consts = require('../../lib/consts/consts');
const config = require('config');
const validator = require('validator');
const mongoose = require('mongoose');
const async = require('async');
const logUtils = require('../../lib/utils/logUtils');
const router = express.Router();

// Get all apps
router.get('/app', validate, function(req, res, next) {
  const page = req.query.page;
  const number = consts.NUMBER_PER_PAGE;
	const skip = number*page;

  globalVariables
    .get('models')
    .App
    .find({})
    .skip(skip)
		.limit(number)
    .sort({createdAt: -1})
    .lean()
    .exec()
    .then(r => {
      logUtils.logInfo(r);

      res.json({
        ec: consts.CODE.SUCCESS,
        apps: r
      });
    })
    .catch(err => {
      next(err);
    });
});

function validate(req, res, next) {
  // Standardize
  let page = req.query.page;
  if(_.isString(page) && validator.isInt(page)) {
    page = _.toInteger(page);
    page = page < 0 ? 0 : page;
  } else {
    page = 0;
  }

  req.query.page = page;
  next();
}

module.exports = function (app) {
  app.use('/', router);
};
