"use strict";

const debug = require('debug')('myapp');
const globalVariables = require('../../lib/utils/global-variables.js');
const utils = require('../../lib/utils/utils.js');
const express = require('express');
const Promise = require('bluebird');
const _ = require('lodash');
const consts = require('../../lib/consts/consts');
const config = require('config');
const validator = require('validator');
const mongoose = require('mongoose');
const async = require('async');
const logUtils = require('../../lib/utils/logUtils.js');

const router = express.Router();

// Modify an app
router.put('/app/:_id', validate, function (req, res, next){
  const _id = req.params._id;
  const name = req.body.name;
  const idApp = req.body.idApp;
  const currentTime = Date.now();

  globalVariables
    .get('models')
    .App
    .update({
      _id: _id
    }, {
      idApp: idApp,
      name: name,
      updatedAt: currentTime
    })
    .exec()
    .then(r => {
      logUtils.logInfo(r);
      
      res.json({
        ec: consts.CODE.SUCCESS
      });
    })
    .catch(err => {
      if (err.name === 'MongoError' && err.code === 11000) {
        return res.json({
          ec: consts.CODE.FAIL
        });
      }

      next(err);
    });
});

function validate(req, res, next) {
  const _id = req.params._id;
  const name = req.body.name;
  const idApp = req.body.idApp;

  if(!validator.isMongoId(_id) || !_.isInteger(idApp) || !(_.isString(name) && validator.isLength(name.trim(), 1))) {
    res.json({
      ec: consts.CODE.WRONG_PARAM
    });

    return next('route');
  }
  // Standardize
  req.body.idApp = _.toInteger(idApp);
  req.body.name = name.trim();
  next();
}

module.exports = function (app) {
  app.use('/', router);
};
