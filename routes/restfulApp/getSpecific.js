'use strict';

const globalVariables = require('../../lib/utils/global-variables.js');
const utils = require('../../lib/utils/utils.js');
const express = require('express');
const Promise = require('bluebird');
const _ = require('lodash');
const consts = require('../../lib/consts/consts');
const config = require('config');
const validator = require('validator');
const mongoose = require('mongoose');
const async = require('async');
const logUtils = require('../../lib/utils/logUtils.js');
const router = express.Router();

// Get info about an app
router.get('/app/:_id', validate, function(req, res, next) {
  const _id = req.params._id;
  let appInf = null;

  getAppInf(_id)
    .then(app => {
      logUtils.logInfo(app);

      if(app !== null) {
        appInf = app;
        const idApp = app.idApp;
        return getPlatformInf(idApp);
      } else {
        return Promise.reject({
          ec: consts.CODE.FAIL
        });
      }
    })
    .then(platformsInf => {
      logUtils.logInfo(platformsInf);

      const data = _.merge(appInf, platformsInf);
      res.json(data);
    })
    .catch(err => {
      if(_.isError(err)) {
        return next(err);
      }

      res.json(err);
    });
});

// Middleware for checking data
function validate(req, res, next) {
  const _id = req.params._id;

  if(!validator.isMongoId(_id)) {
    res.json({
      ec: consts.CODE.WRONG_PARAM
    })
    return next('route');
  }

  next();
}

function getAppInf(_id) {
  return globalVariables
      .get('models')
      .App
      .findOne({
        _id: _id
      })
      .lean()
      .exec();
}

function getIOSInfo(idApp) {
  return globalVariables
      .get('models')
      .AppleCertKey
      .count({
        idApp: idApp
      })
      .exec();
}

function getAndroidInfo(idApp) {
  return new Promise((resolve, reject) => {
    globalVariables
      .get('models')
      .AndroidApiKey
      .count({
        idApp: idApp
      })
      .exec()
      .then(result => {
        result = (result !== 0) ? 1 : 0;
        resolve(result);
      })
      .catch(err => {
        reject(err);
      })
  });
}

function getWinphoneInfo(idApp) {
  return new Promise((resolve, reject) => {
    globalVariables
      .get('models')
      .WindowsPhoneURI
      .count({
        idApp: idApp
      })
      .exec()
      .then(result => {
        result = (result !== 0) ? 1 : 0;
        resolve(result);
      })
      .catch(err => {
        reject(err);
      })
  });
}

function getPlatformInf(idApp) {
  return new Promise((resolve, reject) => {
    async.parallel({
      iOS: function(callback) {
        getIOSInfo(idApp)
          .then(result => {
            callback(null, result);
          })
          .catch(err => {
            callback(err);
          });
      },
      android: function(callback) {
        getAndroidInfo(idApp)
          .then(result => {
            result = (result !== 0) ? 1 : 0;
            callback(null, result);
          })
          .catch(err => {
            callback(err);
          });
      },
      windowsPhone: function(callback) {
        getWinphoneInfo(idApp)
          .then(result => {
            result = (result !== 0) ? 1 : 0;
            callback(null, result);
          })
          .catch(err => {
            callback(err);
          });
      }
    }, function(err, r) {
      if(err) {
        return reject(err);
      }

      resolve(r);
    });
  });
}

module.exports = function (app) {
  app.use('/', router);
};
