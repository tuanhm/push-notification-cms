'use strict';

const debug = require('debug')('myapp');
const globalVariables = require('../../lib/utils/global-variables.js');
const express = require('express');
const Promise = require('bluebird');
const _ = require('lodash');
const consts = require('../../lib/consts/consts');
const config = require('config');
const validator = require('validator');
const mongoose = require('mongoose');
const async = require('async');
const logUtils = require('../../lib/utils/logUtils.js');

const router = express.Router();

router.get('/android/:idApp', validate, function(req, res, next) {
  const idApp = req.params.idApp;

  globalVariables
    .get('models')
    .AndroidApiKey
    .findOne({
      idApp: idApp
    }, {
      _id: 0
    })
    .lean()
    .exec()
    .then(r => {
      logUtils.logInfo(r);

      res.json({
        ec: consts.CODE.SUCCESS,
        inf: r
      });
    })
    .catch(err => {
      next(err);
    });
});

function validate(req, res, next) {
  const idApp = req.params.idApp;

  if(!validator.isInt(idApp)) {
    res.json({
      ec: consts.CODE.WRONG_PARAM
    });

    return next('route');
  }

  // Standardize
  req.params.idApp = _.toInteger(idApp);

  next();
}

module.exports = function (app) {
  app.use('/', router);
};
