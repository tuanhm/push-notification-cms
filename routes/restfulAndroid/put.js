'use strict';

const debug = require('debug')('myapp');
const globalVariables = require('../../lib/utils/global-variables.js');
const utils = require('../../lib/utils/utils.js');
const express = require('express');
const Promise = require('bluebird');
const _ = require('lodash');
const consts = require('../../lib/consts/consts');
const config = require('config');
const validator = require('validator');
const mongoose = require('mongoose');
const async = require('async');
const logUtils = require('../../lib/utils/logUtils.js');

const router = express.Router();

router.put('/android/:idApp', validate, function (req, res, next){
  const idApp = req.params.idApp;
  const currentTime = Date.now();

  globalVariables
    .get('models')
    .AndroidApiKey
    .update({
      idApp: idApp
    }, {
      apiKey: apiKey,
      updatedAt: currentTime
    })
    .exec()
    .then(r => {
      logUtils.logInfo(r);

      res.json({
        ec: consts.CODE.SUCCESS
      });
    })
    .catch(err => {
      next(err);
    })
});

function validate(req, res, next) {
  const idApp = req.params.idApp;
  const apiKey = req.body.apiKey;

  if(!validator.isInt(idApp) || !(_.isString(apiKey) && validator.isLength(apiKey.trim(), 1))) {
    res.json({
      ec: consts.CODE.WRONG_PARAM
    });

    return next('route');
  }

  // Standardize
  req.params.idApp = _.toInteger(idApp);
  req.body.apiKey = apiKey.trim();
  next();
}

module.exports = function (app) {
  app.use('/', router);
};
