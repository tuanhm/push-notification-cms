'use strict';

const debug = require('debug')('myapp');
const globalVariables = require('../../lib/utils/global-variables.js');
const utils = require('../../lib/utils/utils.js');
const express = require('express');
const Promise = require('bluebird');
const _ = require('lodash');
const consts = require('../../lib/consts/consts');
const config = require('config');
const validator = require('validator');
const mongoose = require('mongoose');
const async = require('async');
const logUtils = require('../../lib/utils/logUtils.js');

const router = express.Router();

router.post('/android/:idApp', validate, function (req, res, next){
  const idApp = req.params.idApp;
  const apiKey = req.body.apiKey;
  const currentTime = Date.now();
  let token = req.body.token;
  let runTest = req.body.runTest;


  globalVariables
    .get('models')
    .AndroidApiKey
    .create({
      idApp: idApp,
      apiKey: apiKey,
      createdAt: currentTime,
      updatedAt: currentTime,
      runTest: runTest,
      token: token
    })
    .then(r => {
      logUtils.logInfo(r);

      res.json({
        ec: consts.CODE.SUCCESS
      });
    })
    .catch(err => {
      if (err.name === 'MongoError' && err.code === 11000) {
        return res.json({
          ec: consts.CODE.FAIL
        });
      }

      next(err);
    });
});

function validate(req, res, next) {
  const idApp = req.params.idApp;
  const apiKey = req.body.apiKey;
  let runTest = req.body.runTest;
  let token = req.body.token;

  if(!validator.isInt(idApp) || !(_.isString(apiKey) && validator.isLength(apiKey.trim(), 1))) {
    res.json({
      ec: consts.CODE.WRONG_PARAM
    });

    return next('route');
  }

  // Standardize
  req.params.idApp = _.toInteger(idApp);
  req.body.apiKey = apiKey.trim();

  if(!_.isPlainObject(token)) {
    token = {
      type: 'local'
    };
  } else {
    if(token.type !== 'local' && token.type !== 'remote') {
      token.type = 'local';
    }
  }
  if(runTest !== 0 && runTest !== 1) {
    runTest = 1;
  }

  req.body.token = token;
  req.body.runTest = runTest;
  next();
}

module.exports = function (app) {
  app.use('/', router);
};
