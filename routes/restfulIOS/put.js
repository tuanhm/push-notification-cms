"use strict";

const globalVariables = require('../../lib/utils/global-variables.js');
const utils = require('../../lib/utils/utils.js');
const fileUpload = require('express-fileupload');
const express = require('express');
const Promise = require('bluebird');
const formidable = require('formidable');
const _ = require('lodash');
const consts = require('../../lib/consts/consts');
const config = require('config');
const validator = require('validator');
const mongoose = require('mongoose');
const async = require('async');
const fs = require('fs');
const logUtils = require('../../lib/utils/logUtils.js');

const router = express.Router();

router.put('/ios/:idApp/:_id',
  fileUpload({
    limits: { fileSize: consts.MAX_FILE_SIZE },
    files: 2
  }),
  validate,
  checkExist,
  saveFile,
  function (req, res, next){
    const _id = req.params._id;
    const idApp = req.params.idApp;
    const bundleId = req.body.bundleId;
    const passphrase = req.body.passphrase;
    const currentTime = Date.now();

    globalVariables
      .get('models')
      .AppleCertKey
      .update({
        _id: _id
      }, {
        idApp: idApp,
        bundleId: bundleId,
        updatedAt: currentTime,
        passphrase: passphrase
      })
      .exec()
      .then(r => {
        res.json({
          ec: consts.CODE.SUCCESS,
        });
      })
      .catch(err => {
        if (err.name === 'MongoError' && err.code === 11000) {
          return res.json({
            ec: consts.CODE.FAIL
          });
        }
        next(err);
      });
  }
);

// Middleware for checking data
function validate(req, res, next) {
  req.files = req.files || {};
  Object.keys(req.files).forEach(fileName => {
    logUtils.logInfo(`fileName: ${fileName}`);
  });

  const certFile = req.files.cert;
  const keyFile = req.files.key;
  const idApp = req.params.idApp;
  const _id = req.params._id;
  const bundleId = req.body.bundleId;
  const passphrase = req.body.passphrase;

  if(!validator.isMongoId(_id)
  || !validator.isInt(idApp)
  || !(_.isString(bundleId) && validator.isLength(bundleId.trim(), 1))
  || !(_.isString(passphrase) && validator.isLength(passphrase.trim(), 1))) {
    res.json({
      ec: consts.CODE.WRONG_PARAM
    });

    return next('route');
  }

  // Standardize
  req.body.bundleId = bundleId.trim();
  req.body.passphrase = passphrase.trim();
  req.params.idApp = _.toInteger(idApp);
  next();
}

function checkExist(req, res, next) {
  const idApp = req.params.idApp;
  const bundleId = req.body.bundleId;
  const _id = req.params._id;

  globalVariables
    .get('models')
    .AppleCertKey
    .findOne({
      idApp: idApp,
      bundleId: bundleId
    }, {
      _id: 1
    })
    .lean()
    .exec()
    .then(r => {
      if(!r || r._id === _id) {
        return next();
      }
      
      res.json({
        ec: consts.CODE.FAIL
      });
      next('route');
    })
    .catch(err => {
      next(err);
    });
}

// Middleware for save file
function saveFile(req, res, next) {
  const pathSave = utils.pathSave;
  const idApp = req.params.idApp;
  const bundleId = req.body.bundleId;
  const prePath = `${pathSave}/${idApp}`;
  fs.existsSync(prePath) || fs.mkdirSync(prePath);

  async.parallel([
    function (callback) {
      if(_.isUndefined(req.files.cert)) {
        callback(null);
      } else {
        const path = `${prePath}/${bundleId}.cert`;

        req.files.cert.mv(path , err => {
          if(err) {
            return callback(err);
          }
          callback(null);
        });
      }
    },
    function (callback) {
      if(_.isUndefined(req.files.cert)) {
        callback(null);
      } else {
        const path = `${prePath}/${bundleId}.key`;

        req.files.key.mv(path , err => {
          if(err) {
            return callback(err);
          }
          callback(null);
        });
      }
    },
  ], function (err) {
    if(err) {
      return next(err);
    }
    next();
  });
}

module.exports = function (app) {
  app.use('/', router);
};
