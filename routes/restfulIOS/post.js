"use strict";

const debug = require('debug')('myapp');
const globalVariables = require('../../lib/utils/global-variables.js');
const utils = require('../../lib/utils/utils.js');
const fileUpload = require('express-fileupload');
const express = require('express');
const Promise = require('bluebird');
const formidable = require('formidable');
const _ = require('lodash');
const consts = require('../../lib/consts/consts');
const config = require('config');
const validator = require('validator');
const mongoose = require('mongoose');
const async = require('async');
const fs = require('fs');
const logUtils = require('../../lib/utils/logUtils.js');

const router = express.Router();

router.post('/ios/:idApp',
  fileUpload({
    limits: { fileSize: consts.MAX_FILE_SIZE },
    files: 2
  }),
  validate,
  checkExist,
  saveFile,
  function (req, res, next){
    const idApp = req.params.idApp;
    const bundleId = req.body.bundleId;
    const passphrase = req.body.passphrase;
    const env = req.body.env;
    let token = req.body.token;
    let authType = req.body.authType;
    let runTest = req.body.runTest;
    const currentTime = Date.now();

    globalVariables
      .get('models')
      .AppleCertKey
      .create({
        idApp: idApp,
        bundleId: bundleId,
        createdAt: currentTime,
        updatedAt: currentTime,
        passphrase: passphrase,
        token: token,
        env: env,
        authType: authType,
        runTest: runTest
      })
      .then(r => {
        logUtils.logInfo(r);

        res.json({
          ec: consts.CODE.SUCCESS,
          _id: r._id
        })
      })
      .catch(err => {
        if (err.name === 'MongoError' && err.code === 11000) {
          return res.json({
            ec: consts.CODE.FAIL
          });
        }
        next(err);
      });
  }
);

// Middleware for checking data
function validate(req, res, next) {
  req.files = req.files || {};
  Object.keys(req.files).forEach(fileName => {
    logUtils.logInfo(`fileName: ${fileName}`);
  });

  const certFile = req.files.cert;
  const keyFile = req.files.key;
  const idApp = req.params.idApp;
  const bundleId = req.body.bundleId;
  const passphrase = req.body.passphrase;
  const env = req.body.env;
  let token = req.body.token;
  let authType = req.body.authType;
  let runTest = req.body.runTest;

  if(_.isUndefined(certFile)
  || _.isUndefined(keyFile)
  || !validator.isInt(idApp)
  || !(_.isString(bundleId) && validator.isLength(bundleId.trim(), 1))
      || !(_.isString(authType) && validator.isLength(authType.trim(), 1))
  || !(_.isString(passphrase) && validator.isLength(passphrase.trim(), 1))) {
    res.json({
      ec: consts.CODE.WRONG_PARAM
    });

    return next('route');
  }

  // Standardize
  req.body.bundleId = bundleId.trim();
  req.body.passphrase = passphrase.trim();
  req.body.authType = authType.trim();
  req.params.idApp = _.toInteger(idApp);
  if(env !== 'production' && env !== 'development') {
    req.body.env = 'development';
  }

  if(!_.isPlainObject(token)) {
    token = {
      type: 'local'
    };
  } else {
    if(token.type !== 'local' && token.type !== 'remote') {
      token.type = 'local';
    }
  }
  req.body.token = token;

  if(runTest !== 0 && runTest !== 1) {
    runTest = 1;
  }
  req.body.runTest = runTest;
  next();
}

// Middleware for check exists
function checkExist(req, res, next) {
  const idApp = req.params.idApp;
  const bundleId = req.body.bundleId;

  globalVariables
    .get('models')
    .AppleCertKey
    .count({
      idApp: idApp,
      bundleId: bundleId
    })
    .then(r => {
      if(r !== 0) {
        res.json({
          ec: consts.CODE.FAIL
        })
        return next('route');
      }
      next();
    })
    .catch(err => {
      next(err);
    });
}

// Middleware for save file
function saveFile(req, res, next) {
  const pathSave = utils.pathSave;
  const idApp = req.params.idApp;
  const bundleId = req.body.bundleId;
  const prePath = `${pathSave}/${idApp}`;
  fs.existsSync(prePath) || fs.mkdirSync(prePath);

  async.parallel([
    function (callback) {
      const path = `${prePath}/${bundleId}.cert`;

      req.files.cert.mv(path , err => {
        if(err) {
          return callback(err);
        }
        callback(null);
      });
    },
    function (callback) {
      const path = `${prePath}/${bundleId}.key`;

      req.files.key.mv(path , err => {
        if(err) {
          return callback(err);
        }
        callback(null);
      });
    },
  ], function (err) {
    if(err) {
      return next(err);
    }
    next();
  });
}

module.exports = function (app) {
  app.use('/', router);
};
