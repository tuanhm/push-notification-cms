'use strict';

const debug = require('debug')('myapp');
const globalVariables = require('../../lib/utils/global-variables.js');
const utils = require('../../lib/utils/utils.js');
const express = require('express');
const Promise = require('bluebird');
const _ = require('lodash');
const consts = require('../../lib/consts/consts');
const config = require('config');
const validator = require('validator');
const mongoose = require('mongoose');
const async = require('async');
const logUtils = require('../../lib/utils/logUtils.js');

const router = express.Router();

// Get all apps
router.get('/ios/:idApp', function(req, res, next) {
  const page = req.query.page;
  const number = consts.NUMBER_PER_PAGE;
	const skip = number*page;

  const idApp = req.params.idApp;

  globalVariables
    .get('models')
    .AppleCertKey
    .find({
      idApp: idApp
    })
    .then(r => {
      logUtils.logInfo(r);

      res.json({
        ec: consts.CODE.SUCCESS,
        builds: r
      });
    })
    .catch(err => {
      next(err);
    });
});

function validate(req, res, next) {
  const idApp = req.params.idApp;
  if(!validator.isInt(idApp)) {
    res.json({
      ec: consts.CODE.WRONG_PARAM
    });
    return next('route');
  }

  // Standardize
  let page = req.query.page;
  if(validator.isInt(page)) {
    page = _.toInteger(page);
  } else {
    page = 0;
  }

  req.query.page = page;
  req.params.idApp = _.toInteger(idApp);
  next();
}

module.exports = function (app) {
  app.use('/', router);
};
