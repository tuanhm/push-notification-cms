"use strict";

const debug = require('debug')('myapp');
const config = require('config');
const express = require('express');
const bodyParser = require('body-parser');
const methodOverride = require('method-override');
const timeout = require('connect-timeout');
const morgan = require('morgan');
const responseTime = require('response-time');
const consts = require('./lib/consts/consts');
const globalVariables = require('./lib/utils/global-variables.js');
const utils = require('./lib/utils/utils.js');
const logUtils = require('./lib/utils/logUtils.js')
const winston = require('winston');
const fs = require('fs');
const util = require('util');

// Log API HTTP
let fileStreamRotator = require('file-stream-rotator');
let logDirectory = __dirname + '/access-logs';
fs.existsSync(logDirectory) || fs.mkdirSync(logDirectory);
let accessLogStream = fileStreamRotator.getStream({
	filename: logDirectory + '/access-%DATE%.log',
	frequency: 'daily',
	verbose: false,
	date_format: "YYYY-MM-DD-HH"
});

// Log ERROR
let logSystemDir = __dirname + '/system-logs';
fs.existsSync(logSystemDir) || fs.mkdirSync(logSystemDir);
const logger = new winston.Logger({
  levels: {
    info: 1,
    error: 0,
  },
  colors: {
    info: 'green',
    error: 'red'
  },
  transports: [
    new (winston.transports.Console)({
      level: 'info',
      colorize: true
    }),
    new (require('winston-daily-rotate-file'))({
      level: 'error',
			datePattern: 'dd-MM-yyyy',
			filename: logSystemDir + '/system-',
			json: false,
			timestamp: function () {
        return (new Date()).toLocaleString();
      },
		})
  ]
});

// global variables
globalVariables.set('models', require('./lib/models'));
globalVariables.set('rabbitmq', require('./lib/connections/rabbitmq/rabbitmq.js'));
globalVariables.set('logger', logger);

let routes = require('./routes');
let app = express();

// Create directory for save cert, key
const pathSave = config.pathSave;
utils.handlePathSave(__dirname, pathSave, app);

app.use(timeout('5s'));
app.use(morgan('combined', {stream: accessLogStream}));
app.use(morgan('dev'));
app.use(responseTime());
app.use(bodyParser.json());
app.use(showArguments);
app.use(methodOverride());
app.use(haltOnTimedout);

// router
routes(app);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	let err = new Error('Not Found');
	err.status = 404;
	next(err);
});

// catch error
app.use(function(err, req, res, next) {
  console.log(err);
	const status = utils.getStatus(err.status);
	res.status(status);
	res.json({
		message: err.message,
		error: utils.getDescriptionError(err),
		ec: consts.CODE.ERROR
	});

	if(err.status !== 404) {
		logUtils.logError(`Method: ${req.method} URL: ${req.originalUrl} \n Params: ${util.inspect(req.params)} \n Query: ${util.inspect(req.query)} \n Body: ${util.inspect(req.body)}`, err);
	}
});

// Use for timeout a request
function haltOnTimedout(req, res, next) {
	if (!req.timedout) next();
}

function showArguments(req, res, next) {
  globalVariables
    .get('logger')
    .info(`query: ${util.inspect(req.query)} body: ${util.inspect(req.body)}`);
  next(); 
}

// uncaught error
process.on('uncaughtException', function(err) {
	logUtils.logError(`uncaughtException:`, err);
});

module.exports = app;
