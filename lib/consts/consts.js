module.exports = {
	CODE: {
		FAIL: 0,
		SUCCESS: 1,
		WRONG_PARAM: 2,
		ERROR: 3
	},
	PUSH_STATE: {
		PENDDING: 2,
		SUCCESS: 3,
		ERROR: 4,
	},
	NUMBER_PER_PAGE: 10,
	MAX_FILE_SIZE: 50*1024*1024, // bytes
	APPLE: {
	},
	WINDOWS_PHONE: {
	},
	ANDROID: {
		URL_TOPIC_ANDROID_PATTERN: 'https://iid.googleapis.com/iid/v1/%s/rel/topics/%s'
	},
}
