"use strict";

const debug = require('debug')('myapp');
const schedule = require('node-schedule');
const globalVariables = require('../utils/global-variables.js');
const mongoConnection = require('../connections/mongo/mongodb.js');
const EventEmitter = require('events').EventEmitter;
const consts = require('../consts/consts');
const logUtils = require('../utils/logUtils.js');
const jobUtils = require('../utils/jobUtils.js');
const config = require('config');
const ResourceReady = require('../utils/resourceReadyUtils.js');
const util = require('util');

class SchedulePush extends EventEmitter {
  constructor() {
    super();

    this.listJobs = {};
    this.queueName = config.rabbitmq.queueName;

    this.init();
  }

  addJob(id, jobInstance) {
    this.listJobs[id] = jobInstance;
    return this;
  }

  removeJob(id) {
    delete this.listJobs[id];
    return this;
  }

  getJob(id) {
    return this.listJobs[id];
  }

  init() {
    this.on('job created', this.newJob);
    this.on('job modified', this.jobMofified);
    this.on('job removed', this.jobRemoved);

    this.restoreJobState();
  }

  /*
   * use to re-schedule job is valid to push
   */
  restoreJobState() {
    const resourceReady = new ResourceReady([{
      instance: globalVariables.get('rabbitmq'),
      eventName: 'ready'
    }, {
      instance: mongoConnection,
      eventName: 'connected'
    }]);

    resourceReady.on('ready', () => {
      globalVariables
        .get('models')
        .PushNotification
        .find({
          status: consts.PUSH_STATE.PENDDING
        })
        .lean()
        .exec()
        .then(pushes => {
          logUtils.logInfo(`Init ${pushes.length} pushes!`);

          pushes.forEach(push => {
            logUtils.logInfo(push);

            const jobInf = {
              idJob: push._id.str,
              timeStart: push.timeStart
            }

            this.scheduleJob(jobInf);
          });
        })
        .catch(err => {
          logUtils.logError('Job init', err);
        });
    });

    resourceReady.setUp();
  }

  scheduleJob(jobInf) {
    const currentTime = Date.now();
    const timeStart = jobInf.timeStart;
    const idJob = jobInf.idJob;

    if (currentTime >= timeStart) {
      this.sendJob(idJob);
    } else {
      const jobInstance = schedule.scheduleJob(new Date(timeStart), () => {
        logUtils.logInfo(`Job ${idJob} has started`);

        this.sendJob(idJob);
        this.removeJob(idJob);
      });

      this.addJob(idJob, jobInstance);
    }
  }

  sendJob(idJob) {
    let statusJob = consts.PUSH_STATE.SUCCESS;
    try {
      globalVariables
        .get('rabbitmq')
        .getChannel()
        .sendToQueue(this.queueName, new Buffer(idJob), {
            persistent: true
          },
          (err, r) => {
            if (err) {
              statusJob = consts.PUSH_STATE.ERROR;
              logUtils.logError(`Job sendJob ${idJob}`, err);
            } else {
              logUtils.logInfo(`Job has sent ${idJob}`);
            }
          });
    } catch (err) {
      statusJob = consts.PUSH_STATE.ERROR;
      logUtils.logError(`Job sendJob ${idJob}`, err);
    } finally {
      jobUtils.updateStatusPush(idJob, statusJob);
    }
  }

  newJob(jobInf) {
    logUtils.logInfo(`New job:`, jobInf);
    this.scheduleJob(jobInf);
    logUtils.logInfo(`Total jobs: ${Object.keys(this.listJobs).length}`);
  }

  jobMofified(jobInf) {
    logUtils.logInfo(`Modify job:`, jobInf);
    this.jobRemoved(jobInf);
    this.scheduleJob(jobInf);
  }

  jobRemoved(jobInf) {
    logUtils.logInfo(`Remove job:`, jobInf);
    const idJob = jobInf.idJob;

    const jobInstance = this.getJob(idJob);
    if (jobInstance !== undefined) {
      jobInstance.cancel();
      this.removeJob(idJob);
    }

    logUtils.logInfo(`Total jobs: ${Object.keys(this.listJobs).length}`);
  }
}

module.exports = new SchedulePush();
