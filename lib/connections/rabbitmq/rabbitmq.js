"use strict";

const config = require('config');
const amqp = require('amqplib');
const consts = require('../../consts/consts.js');
const EventEmitter = require('events').EventEmitter;
const logUtils = require('../../utils/logUtils.js');

class RabbitMQ extends EventEmitter {
	constructor () {
		super();
	}

	connect() {
    const url = 'amqp://' + config.rabbitmq.username + ':' + config.rabbitmq.password
              + '@' + config.rabbitmq.host + ':' + config.rabbitmq.port
              + '/' + config.rabbitmq.vitualHost;
		amqp
			.connect(url)
			.then(conn => {
				console.log('Connected to RabbitMQ');

				this.conn = conn;
				this.createChannel();

        // Listen for some events
				conn.on("close", () => {
					console.log('Disconnected to RabbitMQ');
        });

				conn.on('error', err => {
					logUtils.logError('RabbitMQ', err);
				});
			})
			.catch(err => {
				logUtils.logError('RabbitMQ', err);
			});
	}

	createChannel() {
		this.conn
			.createConfirmChannel()
			.then(ch => {
				console.log('Channel created');
				this.channel = ch;

        const queueName = config.rabbitmq.queueName;

				ch.assertQueue(queueName, {durable: true});
				ch.prefetch(1);

		this.emit('ready');

				ch.on("error", err => {
					logUtils.logError('Channel', err);
        });
			})
			.catch(err => {
				logUtils.logError('Channel', err);
			});
	}

	getConnection() {
		return this.conn;
	}

	getChannel() {
		return this.channel;
	}
}

const rabbitMQ = new RabbitMQ;
rabbitMQ.connect();

module.exports = rabbitMQ;
