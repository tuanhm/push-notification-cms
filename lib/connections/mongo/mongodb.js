"use strict";

let config = require('config');
let mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
const logUtils = require('../../utils/logUtils.js');

let model = mongoose.createConnection('mongodb://' + config.mongodb.host + ':' + config.mongodb.port + '/' + config.mongodb.database, {
  db: {
    bufferMaxEntries: 0, // means that will not save any operator in mongodb driver, just reject right away
  },
  server: {
    reconnectTries: 999999,
    auto_reconnect: true,
    poolSize: 1 // Default is 5 connections it's too much for CMS maybe
  },
  user: config.mongodb.username,
  pass: config.mongodb.password
});

model.on('connected', () => {
  console.log('Connected to MongoDB');
});

model.on('error', err => {
  logUtils.logError('MongoDB', err);
});

model.on('disconnected', () => {
  console.log('Disconnected to MongoDB');
});

module.exports = model;
