"use strict";

const config = require('config');
const redis = require('redis');
const EventEmitter = require('events').EventEmitter;
let logUtils = require('../../utils/logUtils.js');

class Redis extends EventEmitter {
  constructor () {
    super();
  }

  connect() {
    let port = config.redis.port;
    let host = config.redis.host;
    let password = config.redis.password;
    let database = config.redis.database;

    this.conn = redis.createClient(port, host, {
      db: database,
      password: password
    });

    if(password) {
      this.conn.auth(password);
    }

    this.conn.on('connect', () => {
      this.emit('ready');
    });

    this.conn.on('error', (err) => {
    });

    this.conn.on('end', () => {

    });
  }

  getConnection() {
    return this.conn;
  }
}

const singletonInstance = new Redis();
singletonInstance.connect();

module.exports = singletonInstance;
