'use strict';

const globalVariables = require('./global-variables');
const logUtils = require('./logUtils');

function updateStatusPush(idJob, status) {
  globalVariables
    .get('models')
    .PushNotification
    .update({
      _id: idJob
    }, {
      status: status
    })
    .then((r) => {})
    .catch((err) => {
      logUtils.writeLog(`[JOB] updateStatusPush ${idJob} ${status}`, err);
    })
}

module.exports = {
  updateStatusPush: updateStatusPush
}