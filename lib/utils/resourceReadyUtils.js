'use strict';

const EventEmitter = require('events').EventEmitter;

class ResourceReady extends EventEmitter {
  constructor(list) {
    super();

    this.list = list;
  }

  setUp() {
    if(this.list.length) {
      let count = 0;

      this.list.forEach((resource) => {
        const instance = resource.instance;
        const eventName = resource.eventName;

        instance.once(eventName, () => {
          count++;
          if(count === this.list.length) {
            this.emit('ready');
          }
        });
      });
    } else {
      this.emit('ready');
    }
  }
}

module.exports = ResourceReady;