'use strict';

const config = require('config');
const express = require('express');
const fs = require('fs');
const _ = require('lodash');
const validator = require('validator');

const obj = {
  getDescriptionError: function(err) {
    return config.debug ? err.stack : '';
  },
  config: {
    logConsole: {
      debug: function(level = 0, ...args) {
        console.log(...args);
      },
      production: function(level = 0, ...args) {
        if (level === this.constants.FORCE_LOG) {
          console.log(...args);
        }
      }
    }
  },
  constants: {
    FORCE_LOG: 1,
    DEBUG_LOG: 0
  },
  init: function() {
    Object.keys(this.config).forEach(funcName => {
      if(config.debug) {
        this[funcName] = this.config[funcName].debug;
      } else {
        this[funcName] = this.config[funcName].production;
      }
    });
  },
  handlePathSave: function(rootDir, pathSave, app) {
    pathSave = config.pathSave || 'public/apns';
    let parts = pathSave.split('/');
    let current = rootDir;
    parts.forEach(part => {
    	current = current + '/' + part;
    	fs.existsSync(current) || fs.mkdirSync(current);
    });
    this['pathSave'] = rootDir + '/' + pathSave;
    app.use('/static/apns', express.static(this['pathSave']));
  },
  getStatus: function(status) {
    if(!_.isInteger(status)) {
      status = 500
    }
    return status;
  }
}

obj.init();

module.exports = obj;
