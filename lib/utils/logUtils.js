'use strict';

const globalVariables = require('./global-variables');
const config = require('config');
const winston = require('winston');

const obj = {
  logError: (...args) => {
    globalVariables
      .get('logger')
      .error(...args, {});
  },
  init: function() {
    this['logInfo'] = () => {};
    if(config.logLevel === 'info') {
      this['logInfo'] = (...args) => {
        globalVariables
          .get('logger')
          .info(...args, {});
      }
    }
  }
}

obj.init();

module.exports = obj;
