"use strict";

const globalVariables = require('../utils/global-variables.js');
const consts = require('../consts/consts');
const config = require('config');
const maxSize = 10;

class SubRedis {
  constructor() {
    this.channel = config.redis.channel || 'PNTD';
    // this.listAPN = [];
    // this.listGCM = [];
    // this.listURI = [];
  }

  subscribe() {
    const sub = globalVariables.get('redis').getConnection();

    sub.on('subscribe', (channel, count) => {
      console.log(`[Redis] Subscribed to channel: ${channel}`);
    });
    
    sub.on('message', (channel, message) => {
      if(channel === this.channel) {
        try {
          const temp = JSON.parse(message);
          console.log(temp);

          if(temp.os === 4) {
            this.handleApple(temp);
          } else if(temp.os === 2) {
            this.handleAndroid(temp);
            
          } else if(temp.os === 1) {
            // console.log('Windows Phone: Not supported yet');
            this.handleWindowsPhone(temp);
          }
        } catch(err) {
          console.log(err);
        }
      }
    });

    sub.subscribe(this.channel);
  }

  handleApple(obj) {
    const idApp = obj.idApp || '';
    const bundleId = obj.bundleId || 'com.sctvsport.bongdatructuyen';
    const token = obj.token || '';
    const currentTime = Date.now();

    if(idApp && bundleId && token) {
      globalVariables
        .get('models')
        .AppleDeviceToken
        .update({
          idApp: idApp,
          bundleId: bundleId,
          deviceToken: token
        }, {
          lastActivedAt: currentTime
        }, {
          upsert: true,
        })
        .then( (r) => {
          console.log(r);
        })
        .catch( (e) => {
          console.log(e);
        });
    }
  }

  handleAndroid(obj) {
    const idApp = obj.idApp || '';
    const token = obj.token || '';
    const currentTime = Date.now();

    if(idApp && token) {
      globalVariables
        .get('models')
        .AndroidRegistrationToken
        .update({
          idApp: idApp,
          registrationToken: token
        }, {
          lastActivedAt: currentTime
        }, {
          upsert: true,
          writeConcern: {
            w: 0
          }
        })
        .then( (r) => {
          console.log(r);
        })
        .catch( (e) => {
          console.log(e);
        });
    }
  }

  handleWindowsPhone(obj) {
    const idApp = obj.idApp || '';
    const token = obj.token || '';
    const currentTime = Date.now();

    if(idApp && token) {
      globalVariables
        .get('models')
        .WindowsPhoneURI
        .update({
          idApp: idApp,
          URI: token
        }, {
          lastActivedAt: currentTime
        }, {
          upsert: true,
          writeConcern: {
            w: 0
          }
        })
        .then( (r) => {
          console.log(r);
        })
        .catch( (e) => {
          console.log(e);
        });
    }
  }
}

const subRedis = new SubRedis();

subRedis.subscribe();

module.exports = subRedis;
