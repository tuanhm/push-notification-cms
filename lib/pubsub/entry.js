'use strict';

let fs = require('fs');
let globalVariables = require('../utils/global-variables');

// initial global variables
globalVariables.set('models', require('../models'));
globalVariables.set('redis', require('../connections/redis/redis'));

require('./subRedis');

