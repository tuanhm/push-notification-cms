"use strict";

const mongoConnection = require('../connections/mongo/mongodb.js');
const Schema = require('mongoose').Schema;

let AppleDeviceToken = new Schema({
	idApp: { type: Number },
	bundleId: { type: String },
	deviceToken: { type: String },
	lastActivedAt: { type: Number }
}, { versionKey: false});

AppleDeviceToken.index({idApp: 1, bundleId: 1, deviceToken: 1}, {unique: true});
module.exports = mongoConnection.model('AppleDeviceToken', AppleDeviceToken);
