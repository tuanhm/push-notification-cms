"use strict";

const mongoConnection = require('../connections/mongo/mongodb.js');
let Schema = require('mongoose').Schema;

let App = new Schema({
	idApp: { type: Number, unique: true },
	name: { type: String },
	createdAt: { type: Number },
	updatedAt: { type: Number }
}, { versionKey: false});

App.index({createdAt: 1});

module.exports = mongoConnection.model('App', App);
