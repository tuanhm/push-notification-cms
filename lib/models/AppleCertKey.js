"use strict";

const mongoConnection = require('../connections/mongo/mongodb.js');
const Schema = require('mongoose').Schema;

let AppleCertKey = new Schema({
	idApp: { type: Number },
	bundleId: { type: String },
	passphrase: {type: String},
	createdAt: { type: Number },
	updatedAt: { type: Number },
	env: { type: String },
	authType: {type: String},
	runTest: {type: Number},
	token: { type: Schema.Types.Mixed }
}, { versionKey: false});

AppleCertKey.index({idApp: 1, bundleId: 1}, {unique: true});

module.exports = mongoConnection.model('AppleCertKey', AppleCertKey);
