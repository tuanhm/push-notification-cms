"use strict";

const mongoConnection = require('../connections/mongo/mongodb.js');
let Schema = require('mongoose').Schema;

let PushNotification = new Schema({
	idApp: { type: Number },
	oss: { type: Number },
	content: { type: String },
  	title: { type: String },
	timeStart: { type: Number },
	status: { type: Number },
	active: { type: Number },
  	data: {type: Schema.Types.Mixed }
}, { versionKey: false});

PushNotification.index({timeStart: 1});

module.exports = mongoConnection.model('PushNotification', PushNotification);
