"use strict";

const mongoConnection = require('../connections/mongo/mongodb.js');
let Schema = require('mongoose').Schema;

let AndroidApiKey = new Schema({
	idApp: { type: Number, unique: true },
	apiKey: { type: String },
	createdAt: { type: Number },
	updatedAt: { type: Number },
  	status: { type: Number },
	token: {type: Schema.Types.Mixed },
	runTest: {type: Number}
}, { versionKey: false});

AndroidApiKey.index({idApp: 1});

module.exports = mongoConnection.model('AndroidApiKey', AndroidApiKey);
