"use strict";

const mongoConnection = require('../connections/mongo/mongodb.js');
const Schema = require('mongoose').Schema;

let AndroidRegistrationToken = new Schema({
  idApp: { type: Number },
  registrationToken: { type: String },
  lastActivedAt: { type: Number }
}, { versionKey: false});

AndroidRegistrationToken.index({idApp: 1, registrationToken: 1}, {unique: true});
AndroidRegistrationToken.index({createdAt: 1});

module.exports = mongoConnection.model('AndroidRegistrationToken', AndroidRegistrationToken);
