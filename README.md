# Hệ thống Push Notification
Hệ thống cho phép gửi thông báo tới người dùng app/game. Hệ thống bao gồm các thành phần:
* Client: App/Game
* Server:
    * Service Rest API:
        * Rest API cho việc quản lý thông tin app/game
        * Rest API cho việc quản lý push
    * Schedule module: chịu trách nhiệm cho việc lên lịch thực hiện các push
    * Job module: chịu trách nhiệm cho việc triển khai thực hiện push
    * Subscribe module: nhận các thông tin từ các game/app
* CMS:
    * Quản lý thông tin app/game
    * Quản lý thông tin push

### Một số quy định chung:
* Quy định về respose:
    * Dữ liệu trả về dạng JSON
    * Mã lỗi được lưu trong biến `ec`:
        * 0: Thất bại
        * 1: Thành công
        * 2: Sai tham số
        * 3: Lỗi hệ thống
    * Trạng thái của push được lưu trong `status`:
        * 2: Push đang đợi đến giờ thực hiện
        * 3: Push đã thực hiện thành công
        * 4: Push gặp lỗi 
    * Các trường khác tùy thuộc vào từng request mô tả phía dưới
* Quy định về request
    * Base URL: [http://123.30.235.196:2780/](http://123.30.235.196:2780/) 
    * Method: *GET POST PUT DELETE*
    * Path: Tùy thuộc vào từng request mô tả phía dưới
    
### Cơ sở dữ liệu
Hệ thống bao gồm các DB:
* `App`: 
    * idApp: id của app
    * name: tên của app
    * createdAt: thời điểm tạo lập app
    * updatedAt: thời điểm cập nhật gần nhất
* `AppleCertKey`:
    * idApp: id của app/game
    * buildName: tên của phiên bản build
    * certPath: link tới file cert
    * keyPath: link tới file key
    * bundleId: bundle Id
    * passphrase: passphrase
    * createdAt: thời điểm tạo lập
    * updatedAt: thời điểm cập nhật gần nhất
* `AppleDeviceToken`:
    * idApp: id của game/app
    * bundleId: bundle Id
    * deviceToken: device-token của thiết bị cài đặt game/app,
    * createdAt: thời điểm thêm vào hệ thống
* `AndroidApiKey`:
    * idApp: id của app/game
    * apiKey: API Key của app/game đó
    * createdAt: thời điểm tạo lập
    * updatedAt: thời điểm cập nhật gân nhất
* `WindowsPhoneURI`:
    * idApp: id của app/game
    * URI: URI của thiết bị cài đặt game/apa,
    * createdAt: thời điểm thêm vào hệ thống
* `PushNotification`:
    * idPush: id của lần push
    * idApp: id của app/game
    * oss: các hệ điều hành áp dụng
    * content: nội dung chuyển tới người dùng game/app
    * timeStart: thời điểm push
    * timeEnd: thời điểm cuối cùng gửi push
    * status: trạng thái push (đang chờ push, đã push)
    * active: 
* `PushResult`:
    * idPush: id của lần push
    * ios:
        * success: số lượng push thành công
        * fail: số lượng push thất bại
        * totalTime: tổng thời gian
    * android:
        * status: 0|1
        * totalTime: tổng thời gian
        * reason: lý do thất bại
    * windowsPhone:
        * success: số lượng push thành công
        * fail: số lượng push thất bại
        * totalTime: tổng thời gian

## 1. Rest API cho việc quản lý thông tin app/game
#### 1.1 Lấy thông tin về các game/apps trong hệ thống
Người dùng sử dụng để lấy thông tin về các app trong hệ thống. 
* path: `/app`
* method: *GET*
* Response:
    * `ec`: Mã lỗi `3|1`
    * `apps`: Danh sách các app (trong trường hợp không xảy ra lỗi)
        * `_id`: id của hệ thống sinh ra, các request  liên quan đến chỉnh sửa xóa sẽ sử dụng id này để trao đổi với hệ thống
    
EX:
```
{
    "ec": 1,
    "apps": [
        {
            "_id": "5771ec81d979f2b12570fbf6",
            "idApp": 1,
            "name": "aaaaaaa",
            "createdAt": 1467083905182,
            "updatedAt": 1467083905182
        }
    ]
}
```

#### 1.2 Lấy thông tin chi tiết về 1 game/app
Sử dụng để lấy thông tin chi tiết về 1 game/app trong hệ thống, bao gồm thông tin về app/game và thông tin các nền tảng tương ứng.
* path: `/app/:_id`
    * `_id`: id của hệ thống sinh ra cho game/app đó
    
        Ex: Đối với game/app có `_id` là `5771ec81d979f2b12570fbf6` => path tương ứng         như sau: [http://127.0.0.1:8014/app/5771ec81d979f2b12570fbf6]()
* method: `GET`
* Response:
    * `ec`: Mã lỗi `0|1|2|3`
        * 0: `_id` theo path không tồn tại ứng với một app nào trong hệ thống
        * 2: `_id` không đúng định dạng id tự sinh của hệ thống
    * `app`: Thông tin về app/game đó (trong trường hợp thành công)
        * `iOS`: giá trị tương ứng với số phiên bản build trên nền tảng iOS của game/app này
        * `android`:
            * 0: đã cập nhật API Key
            * 1: chưa cập nhật API Key
        * `windowsPhone`:
            * 0: chưa có URI nào đối với game/app này trên nền tảng windows phone
            * 1: đã có URI 

Ex:
```
{
    "ec": 1,
    "app": {
        "_id": "5771ec81d979f2b12570fbf6",
        "idApp": 1,
        "name": "aaaaaaa",
        "createdAt": 1467083905182,
        "updatedAt": 1467083905182,
        "iOS": 2,
        "android": 0,
        "windowsPhone": 1
    }
}
```

#### 1.3 Thêm mới app/game
Sử dụng để thêm mới một game/app vào hệ thống
* path: `/app`
* method: *POST*
* Content-type: `application/json`
* body:
    * `idApp`: id của app, yêu cầu là số nguyên lớn hơn 0
    * `name`: tên của app, không được để trống
* Response:
    * `ec`: Mã lỗi `0|1|2|3`
        * 0: tạo mới app thất bại, do đã có một game/app trong hệ thống trùng tên hoặc trùng id
    * `id`: id của hệ thống cho app/game đó (trong trường hợp thành công)

Ex:
```
{
    "ec": 1,
    "id": "57734e6c35ea633d2a2885f1"
}
```

#### 1.4 Cập nhật app/game
Sử dụng để cập nhật thông tin của game/app đã có trong hệ thống.
* path: `/app/:_id`
    * `_id`: id của hệ thống sinh ra cho game/app đó
    
        Ex: Đối với game/app có `_id` là `5771ec81d979f2b12570fbf6` => path tương ứng         như sau: [http://127.0.0.1:8014/app/5771ec81d979f2b12570fbf6]()
* method: *PUT*
* Content-type: `application/json`
* body:
    * `name`: tên của app (cũ hoặc mới thay đổi)
    * `idApp`: id của app (cũ hoặc mới thay đổi)
* Response:
    * `ec`: Mã lỗi `0|1|2|3`
        * 0: `name` hoặc `idApp` mới cập nhật đã trùng với một app/game có trong hệ thống
        * 2: Sai tham số
            * `_id`: không đúng định dạng id của hệ thống
            * `name`: không được để trống
            * `idApp`: số nguyên lớn hơn 0

#### 1.5 Xóa app/game
Sử dụng để xóa một game/app đã có trong hệ thống
* path: `/app/:_id`
    * `_id`: id của hệ thống sinh ra cho game/app đó
    
        Ex: Đối với game/app có `_id` là `5771ec81d979f2b12570fbf6` => path tương ứng         như sau: [http://127.0.0.1:8014/app/5771ec81d979f2b12570fbf6]()
* method: *DELETE*
* Response:
    * `ec`: Mã lỗi `1|2|3`

#### 1.6 Thêm API Key android
Sử dụng để cập nhật API Key cho  game/app
* path: `/android`
* method: *POST*
* Content-type: `application/json`
* body:
    * `idApp`: id của game/app người dùng tạo
    * `apiKey`: API Key tương ứng của game/app đó, không được để trống
* Response:
    * `ec`: Mã lỗi `0|1|2|3`
        * 0: đã có API Key cập nhật cho game/app này

#### 1.7 Lấy thông tin API Key android
Sử dụng để lấy thông tin API Key của game/app
* path: `/android/:idApp`
    * `:idApp`: là id của game/app đó người dùng nhập vào khi tạo
* method: *GET*
* Response:
    * `ec`: Mã lỗi `1|3`
    * `inf`: thông tin API Key của game/app đó (trong trường hợp thành công)

Ex:
```
{
    "ec": 1,
    "inf": {
        "idApp": 1,
        "apiKey": "AIzaSyCaEoSNLiS8lvQJtEYtX3NIB09GRwl3qQM",
        "createdAt": 1467087633715,
        "updatedAt": 1467087633715
  }
}
```

#### 1.8 Cập nhật API Key android
Sử dụng để cập nhật  API Key
* path: `/android/:idApp`
    * `:idApp`: là id của game/app đó người dùng nhập vào khi tạo
* method: *PUT*
* Content-type: `application/json`
* body:
    * `apiKey`: API Key
* Response:
    * `ec`: Mã lỗi `1|3`
    
#### 1.9 Xóa API Key android
Sử dụng để xóa API Key tương ứng với game/app đó
* path: `/android/:idApp`
* method: *DELETE*
* Response:
    * `ec`: Mã lỗi `1|3`
    
#### 1.10 Thêm bản build ios
Sử dụng để thêm một phiên bản build iOS của game/app này. Một game/app có thể có nhiều bản build khác nhau.
* path: `/apple/:idApp`
    * `:idApp`: là id của game/app đó người dùng nhập vào khi tạo
* method: *POST*
* Content-type: `multipart/form-data`
* headers: 
    * `bundleId`: bundle id
    * `buildName`: tên của phiên bản build, không được để trống
    * `passphrase`: passphrase
* body:
    * `cert`: file cert
    * `key`: file key
* Response:
    * `ec`: Mã lỗi `0|1|2|3`
        * 0: Trùng idBuild hoặc buildName với một phiên bản build của game/app này trong hệ thống
    * `id`: id của hệ thống đặt cho phiên bản build này, các request liên quan tới chỉnh sửa xóa phiên bản build sẽ sử dụng giá trị id này

Ex:
```
{
    "ec": 1,
    "id": "57736fb935ea633d2a2885f4"
}
```

#### 1.11 Lấy thông tin các bản build ios
Sử dụng để lấy thông tin tất cả các bản build tương ứng với game/app này
* path: `/apple/:idApp`
    * `:idApp`: là id của game/app đó người dùng nhập vào khi tạo
* method: *GET*
* Response:
    * `ec`: Mã lỗi `1|3`
    * `builds`: Mảng chứa thông tin các phiên bản build

Ex:
```
{
    "ec": 1,
    "builds": [
        {
            "_id": "577230cce48461174d2b503f",
            "idApp": 1,
            "bundleId": "com.thudo.testnotifi",
            "buildName": "Test",
            "passphrase": "thudo123",
            "keyPath": "/apple/1/2/apple.key",
            "certPath": "/apple/1/2/apple.cert"
        },
        {
            "_id": "57736f9f35ea633d2a2885f2",
            "idApp": 1,
            "bundleId": "com.thudo.testtoo",
            "buildName": "Test",
            "passphrase": "thudo123",
            "keyPath": "/apple/1/3/apple.key",
            "certPath": "/apple/1/3/apple.cert"
        }
    ]
}
```

#### 1.12 Lấy thông tin về một bản build ios
Sử dụng để lấy thông tin về một bản build iOS
* path: `/apple/:idApp/:_id`
    * `:idApp`: id của game/app
    * `_id`: id của phiên bản build (của hệ thống đặt không phải người dùng nhập)
* method: *GET*
* Response:
    * `ec`: Mã lỗi `1|2|3`
    * `build`: thông tin về bản build (trong trường hợp thành công)

Ex:
```
{
    "ec": 1,
    "builds": 
    {
        "_id": "577230cce48461174d2b503f",
        "idApp": 1,
        "bundleId": "com.thudo.testnotification",
        "buildName": "Test",
        "passphrase": "thudo123",
        "keyPath": "/apple/1/2/apple.key",
        "certPath": "/apple/1/2/apple.cert",
    }
}
```

#### 1.13 Cập nhật bản build ios
Sử dụng để thay đổi, cập nhật thông tin của phiên bản build
* path: `/apple/:idApp/:_id`
* method: *PUT*
* Content-type: `multipart/form-data`
* headers:
    * `bundleId`: id phiên bản build
    * `buildName`: tên phiên bản build
    * `passphrase`: passphrase
* body:
    * `cert`: file cert (chỉ gửi trong trường hợp người dùng thay đổi cập nhật file cert, trong trường hợp không thay đổi không gửi file này theo)
    * `key`: file key (chỉ gửi trong trường hợp người dùng thay đổi cập nhật file key, trong trường hợp không thay đổi không gửi param file theo)
* Response:
    * `ec`: Mã lỗi `0|1|2|3`
        * 0: thất bại do đã có một phiên bản build khác của app này trùng tên hoặc trùng bundleId mới cập nhật
        
#### 1.14 Xóa bản build ios
Sử dụng để xóa bản build iOS
* path: `/apple/:idApp/:_id`
* method: *DELETE*
* Response: 
    * `ec`: Mã lỗi `1|3`
    
## 2 Rest API cho việc quản lý push
#### 2.1 Lấy thông tin các push
* path: `/push`
* method: *GET*
* param: page

```
Ex: http://[ip or domain]:[port]/push?page=3
```
* Response: 
    * `ec`: Mã lỗi
    * `pushes`: danh sách các push theo thứ tự từ mới tới cũ

#### 2.2 Thêm push
API cho phép người dùng thêm một thông báo muốn gửi
* path: `/push`
* method: *POST*
* Content-Type: application/json
* body:

    | Name | Optional | Type | Format | Descriptor | Example | 
    | ----- | ------  | ------ | ------ | ---- |  ----- |
    | idApp | **require** | int | 0-9 | id của app/game| 1 |
    | oss | **require** | int | 0-9 | các nền tảng áp dụng| 1 |
    | content | **require** | String | A-Za-z0-9 | nội dung thông báo| Khuyến mại khuyến mại|
    | timeStart | **require** | int | 0-9 | thời điểm gửi (timestamp miliseconds)| 1466673148805|
    | timeEnd | **require** | int | 0-9 | thời điểm cuối cùng thông điệp còn giá trị (timestamp miliseconds)| 1466673148805|
    | title   | **require** | String| A-Za-z0-9 | title của thông báo | iOnline|
    | param   | **require** | String | A-Za-z0-9 | iOnline | '' |
    | link   | **require** | String | A-Za-z0-9 | SCTV Sport | '' |
    | extras   | **require** | JSON | A-Za-z0-9 | SCTV Sport | '' |

* Giá trị của oss:
    * 1: nền tảng windows phone
    * 2: nền tảng android
    * 3: nền tảng windows phone và android
    * 4: nền tảng iOS
    * 5: nền tảng iOS và windows phone
    * 6: nền tảng iOS và Aadroid
    * 7: cả 3 nền tảng

* Giá trị của timeEnd: thời điểm này là thời điểm cuối cùng mà thông điệp muốn gửi tới người dùng còn giá trị, ví dụ 1 tin khuyến mãi trong 1 giờ tuy nhiên gặp vấn đề gì đó mà hệ thống không gửi được trước đó và đã  hết thời gian khuyến mại do đó thông điệp này không nên được gửi tới người dùng nữa
* Response:
    * ec: Mã lỗi `1|2|3`
    * id: id của thông điệp vừa tạo (trong trường hợp thành công)
    
Ex:
```
{
    ec: 1,
    id: "576ba647c35cfad240c3f55b"
}
```

#### 2.3 Chỉnh sửa push
API cho phép người dùng chỉnh sửa một thông báo
* Điều kiện tiên quyết: Thông báo đó chưa được thực hiện, trường hợp người dùng muốn một thông báo tương tự với một thông báo trước đó đã thực hiện, người dùng phải tạo mới với nội dung tương tự
* path: `/push/:id`
* method: *PUT*
* Content-Type: application/json
* body:

    | Name | Optional | Type | Format | Descriptor | Example | 
    | ----- | ------  | ------ | ------ | ---- |  ----- |
    | idApp | **require** | int | 0-9 | id của app/game| 1 |
    | oss | **require** | int | 0-9 | các nền tảng áp dụng| 1 |
    | content | **require** | String | A-Za-z0-9 | nội dung thông báo| Khuyến mại khuyến mại|
    | title   | **require** | String| A-Za-z0-9 | title của thông báo | iOnline|
    | param   | **require** | String | A-Za-z0-9 | iOnline | '' |
    | timeStart | **require** | int | 0-9 | thời điểm gửi (timestamp miliseconds)| 1466673148805|
    | timeEnd | **require** | int | 0-9 | thời điểm cuối cùng thông điệp còn giá trị (timestamp miliseconds)| 1466673148805|
    | link   | **require** | String | A-Za-z0-9 | SCTV Sport | '' |
    | extras   | **require** | JSON | A-Za-z0-9 | SCTV Sport | '' |
    | active | **require** | int | 0 hoặc 1 | deactive hoặc active| 0|

* Tham số id của thông báo push sẽ lấy từ path, ví dụ path cho chỉnh sửa thông báo với `id: 576ba647c35cfad240c3f55b` sẽ là:
    ```
        http://[ip or domain]:[port]/push/576ba647c35cfad240c3f55b
    ```
* Response:
    * ec: Mã lỗi `0|1|2|3`
        * 0: trong trường hợp không tìm thấy push nào tương ứng với id gửi lên, hoặc trạng thái của push đã thực hiện
    
#### 2.4 Xóa push
API cho phép người dùng xóa một thông báo đã có
* path: `/push/:id`
* method: *DELETE*
* Response:
    * ec: Mã lỗi `1|3`

### 3. Subscribe Module
Module chịu trách nhiệm cho việc nhận các thông tin về device-token trong trường hợp là iOS, registration-token trong trường hợp là android, URI trong trường hợp windows phone.

Module sẽ subscribe một channel của redis, các game/app muốn gửi thông tin cho hệ thống sẽ publish các message vào channel này

#### 3.1 Android
* Các thông số cần thiết
    * os: 2
    * idApp: id của app/game
    * token: giá trị của registration-token

Ex:
```
{
    "os": 2,
    "idApp": 1,
    "token": "registration-token here"
}
```

#### 3.2 iOS
* Các thông số cần thiết
    * os: 4
    * idApp: id của app
    * bundleId: bundleId
    * token: device-token

Ex:
```
{
    "os": 4,
    "idApp": 1,
    "bundleId": "com.thudo.testnotifi",
    "token": "device-token here"
}
```

#### 3.3 Windows phone
* Các thông số cần thiết
    * os: 1
    * idApp: id của app
    * token: URI 

Ex:
```
{
    "os": 1,
    "idApp": 1,
    "token": "http://URI-here.com"
}
```
    
